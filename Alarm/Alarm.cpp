/**********************************
* Filename: Alarm.cpp
* 
* author: Stijn
* created: 2018-12-20
* notes:
* 
* desc:
* 
* ver: 2018-12-20 first version
* 
* 
* **********************************/
#include <iostream>
#include <ctime>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <sstream>

#include "SerialPort.h"

#define POLL 2000
void Send(std::string msg);

char portNr[] = "\\\\.\\COM6";
char *port_name = portNr;

char incomingData[MAX_DATA_LENGTH];

SerialPort arduino(port_name);

struct User{
    std::string _username;
    std::string saved_pin;
    std::string _id;
};


int main(){

    //create the file stream
    std::ofstream outFile;
    std::ifstream inFile;
    //variables
    User Users;
    vector<Users> container;
    //time management
    time_t now = time(0);
    char* date_time = ctime(&now);

    //check connection
    if(arduino.isConnected()){
        //print to screen if connected
        std::cout<<"Connection Established"<<std::endl;
    }
    else{
        //print to screen if NOT connected
        std::cout<<"Error! Check port name"<<std::endl;
    }

    //while connected
    while(arduino.isConnected() ){
        //logg the data from arduino
        outFile.open("Alarm_logs.dat", std::ios::app);
        if(outFile.is_open()){
            
            int read_log = arduino.readSerialPort(incomingData, MAX_DATA_LENGTH);
        
            if(read_log > 0 && !'\0'){
                //print data
                std::cout<<"<< " << date_time << ": " << incomingData << std::endl;
                outFile<<"<< " << date_time << ": " << incomingData << std::endl;
            }
            //wait
            Sleep(POLL); //milisec
            outFile.close();
        }
    }
    //compare the entered values with the pre-existing information
    inFile.open("user.dat");
        if(inFile.open()){
            std::string Username, password, id;
            std::string line = " ";
                
            while(!inFile.eof()){
                getline(inFile, Username, ";");
                getline(inFile, password, ";");
                getline(inFile, id);
                container.push_back(Users);
            }
        inFile.close();
        }
    
    for(int i = 0; i < container.length(); i++){
        bool result = false;
        if(entered_pw == Users[i].saved_pin){
            result = true;
            break;
        }
        else
           result = false;
    }
    //send result of comparison to arduino
    Send(result);
    
    return 0;
}

//builds the string to a char array, add a \0 and then send it to the arduino
void Send(std::string msg){

    char outData[msg.length()];
    std::size_t len = msg.copy(outData, msg.length(), 0);
    outData[len] = '\0';
    bool res = arduino.writeSerialPort(outData, sizeof(outData));

}