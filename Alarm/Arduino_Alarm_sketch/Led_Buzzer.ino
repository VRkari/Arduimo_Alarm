void LED_Alarm_Active(){
    //should work but doesnt ... tested all values via Serial.print ... all values are there but to no avail
    if ((millis() - Ms_blink) >= blinker) {
      toggle_Led_Green();
    }
}

void Led_Wrong(){
    digitalWrite(ledPin_red, HIGH);
    delay(1000);
    digitalWrite(ledPin_red, LOW);
}

void Lockdown(){
    Serial.println("Lockdown");
    if((millis() - Ms_blink) >= blinker){
       toggle_Led_Green();
       toggle_Led_Yellow();
       toggle_Led_Red();        
       Ms_blink = millis();
    }

    buzzer_siren();
}

void buzz_correct(){
    //turn on buzzer sequence
    tone(buzzPin, 500);
    delay(150);
    noTone(buzzPin);
    delay(150);
    tone(buzzPin, 500);
    delay(150);
    noTone(buzzPin);
    delay(150);
    //turn of green led
    digitalWrite(ledPin_green, LOW);
}

void buzz_led_option_A(){
    //turn on green LED full
    digitalWrite(ledPin_green, HIGH);
    //3 long beeps
    tone(buzzPin, 500);
    delay(350);
    noTone(buzzPin);
    delay(150);
    tone(buzzPin, 500);
    delay(350);
    noTone(buzzPin);
    delay(150);
    tone(buzzPin, 500);
    delay(350);
    noTone(buzzPin);
}

void buzz_led_option_B(){
    //turn on green LED fade .. doesnt work
    if(Ms_fade - fade_Ms >= fadePeriod){
    analogWrite(ledPin_green, brightness);
    brightness += increment;
    fade_Ms = Ms_fade;
    }
    //2 short beeps
    tone(buzzPin, 500);
    delay(150);
    noTone(buzzPin);
    delay(150);
    tone(buzzPin, 500);
    delay(150);
    noTone(buzzPin);
}

void toggle_Led_Green(){
    if(digitalRead(ledPin_green) == LOW)
        digitalWrite(ledPin_green, HIGH);
    else
        digitalWrite(ledPin_green, LOW);
}

void toggle_Led_Yellow(){
    if(digitalRead(ledPin_yellow) == LOW)
        digitalWrite(ledPin_yellow, HIGH);
    else
        digitalWrite(ledPin_yellow, LOW);
}

void toggle_Led_Red(){
    if(digitalRead(ledPin_red) == LOW)
        digitalWrite(ledPin_red, HIGH);
    else
        digitalWrite(ledPin_red, LOW);
}

void buzzer_siren(){
    
    for(int i = 0; i < 255; i += 2){
      analogWrite(buzzPin, i);
      delay(10);
      if(i = 255){
        for(i = 255; i > 0; i -= 2){
          analogWrite(buzzPin, i);
          delay(10);
        }
      }
    }
}
