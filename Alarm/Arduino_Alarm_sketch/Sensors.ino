bool Both_sensors_trigger(){
    //if any of the triggers go off, go in lockdown mode

    red_Button = digitalRead(redbutton); //outside alarm
    green_Button = digitalRead(greenbutton); //complete alarm
    
    if(red_Button == LOW || green_Button == LOW){
      Serial.println("***A sensor has been activated***");
      sensor_activation_complete_alarm();
      delay(500);
      return true;
    }
    return false;
}

bool Outside_sensor_trigger(){
    //if the outside sensors gets triggered: lockdown

    red_Button = digitalRead(redbutton);

    if(red_Button == LOW){
      Serial.println("***An outside sensor has been activated***");
      sensor_activation_outside_alarm();
      delay(500);
      return true;
    }
    return false;
}

void sensor_activation_complete_alarm(){
    int i = 0;
    //red LED blinker ... doesnt work
    if (Ms_blinkB - blinkB_Ms >= blinker) {
      // save the last time you blinked the LED
      digitalWrite(ledPin_red, !digitalRead(ledPin_red));
      blinkB_Ms = Ms_blinkB;
    }
    
    //buzzer for 3 sec
    while(i < 3000){
      tone(buzzPin, 500);
      delay(150);
      noTone(buzzPin);
      delay(150);
      i += 300;
    }
    digitalWrite(ledPin_yellow, HIGH);
    digitalWrite(ledPin_green, HIGH);
}

void sensor_activation_outside_alarm(){
    int i = 0;
    //red LED blinker
    if (Ms_blinkC - blinkC_Ms >= blinker) {
      // save the last time you blinked the LED
      digitalWrite(ledPin_red, !digitalRead(ledPin_red));
      blinkC_Ms = Ms_blinkC;
    }
    //buzzer for 3 sec ... is an endless loop)
    while(i < 3000){
      tone(buzzPin, 500);
      delay(150);
      noTone(buzzPin);
      delay(150);
      i += 300;
    }
    //turn on yellow and green fully after buzzer
    digitalWrite(ledPin_yellow, HIGH);
    digitalWrite(ledPin_green, HIGH);
}
