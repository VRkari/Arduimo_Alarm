#include <Keypad.h>

//********Function List********
void system_Armed_complete();
void system_Armed_outside();
void system_Unarmed();
void Lockdown();
void Led_Wrong();
void LED_Alarm_Active();
boolean Outside_sensor_trigger();
boolean Both_sensors_trigger();
void buzz_correct();
void buzz_led_option_A();
void buzz_led_option_B();
void sensor_activation_complete_alarm();
void sensor_activation_outside_alarm();
void buzzer_siren();
void toggle_Led_Red();
void toggle_Led_Yellow();
void toggle_Led_Green();

//hardcoded pincode for the alarm
String password = "5382";

//********keypad declaration********
const byte ROWS = 4;
const byte COLS = 4;

char keys[ROWS][COLS] = {
  {'1','2','3', 'A'},
  {'4','5','6', 'B'},
  {'7','8','9', 'C'},
  {'*','0','#', 'D'}
};

byte rowPins[ROWS] = {6, 7, 8, 9}; 
byte colPins[COLS] = {2, 3, 4, 5}; 

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

//********Pin defines********
#define redbutton A0    //outside alarm
#define greenbutton A1  //complete alarm
#define ledPin_green 13
#define ledPin_yellow 12
#define ledPin_red 11
#define buzzPin 10

//********Variable List********
byte red_Button = 0;
byte green_Button = 0;
unsigned long Ms_blink, Ms_fade, Ms_blinkB, Ms_blinkC;
unsigned long blink_Ms = 0;
unsigned long blinkB_Ms = 0;
unsigned long blinkC_Ms = 0;
unsigned long fade_Ms = 0;
byte ledState = HIGH;
unsigned long blinker = 100;
byte attempts;
char key;
String temp_Password = "";
char key_pressed;
boolean check_password = false;
byte alarm_system = 1; //1 is on, 0 is off
const unsigned long fadePeriod = 10;
byte brightness = 0;
byte increment = 1;

//********Program code********
void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);
    pinMode(buzzPin, OUTPUT);
    pinMode(redbutton, INPUT);
    digitalWrite(redbutton, HIGH);
    pinMode(greenbutton, INPUT);
    digitalWrite(greenbutton, HIGH);
    digitalWrite(buzzPin, LOW);
    pinMode(ledPin_green, OUTPUT);
    pinMode(ledPin_yellow, OUTPUT);
    pinMode(ledPin_red, OUTPUT);
}

void loop() {
    // put your main code here, to run repeatedly:
    Ms_fade = millis();
    Ms_blinkB = millis();
    Ms_blinkC = millis();
    //if the alarm is on, run:
    //this loop causes my LEDs and log file to either not blink or endlessly print the same stuff :( 
    system_is_armed();
    //if the alarm is off, run:
    if(alarm_system == 0){
      system_Unarmed();
    }
}
//start state and if alarmed with A
void system_Armed_complete(){
    
    retry_1:
    temp_Password = ""; //empty temp_password
    check_password = false;
    delay(150);
    Serial.println("***Total Alarm is active***");
    Serial.flush();
    
 
    while(!check_password){
      if(!Both_sensors_trigger()){ //if any of the triggers go off, go in lockdown mode
        //deactivate alarm by entering code
        key_pressed = keypad.getKey();
        //entering only numbers into temp_password
        if(key_pressed != NO_KEY){
          if( key_pressed == '0' || key_pressed == '1' || key_pressed == '2' || 
              key_pressed == '3' || key_pressed == '4' || key_pressed == '5' || 
              key_pressed == '6' || key_pressed == '7' || key_pressed == '8' || 
              key_pressed == '9'){
                temp_Password += key_pressed;
                Serial.print(key_pressed);
                Serial.flush();
                //tests
                /*Serial.println("____");
                Serial.println(temp_Password);
                Serial.println("____");*/
                //a small input delay
                delay(250);
          }
          //check the password when # is entered
          else if(key_pressed == '#'){
            //appearently having " " compared to "" enters an invisible white space making comparison impossible ... not shown on monitor
            if(password.equals(temp_Password)){
              Serial.println("Correct pincode");
              Serial.flush();
              alarm_system = 0; //turn off alarm
              buzz_correct();
              attempts = 0;
              check_password = true;
              Serial.println("***Alarm deactivated***");
              Serial.flush();
              break;
            }
            else if(password != temp_Password && attempts < 2){
              Serial.println("Incorrect pincode");
              Serial.flush();
              temp_Password = "";
              Led_Wrong();
              attempts += 1;
              goto retry_1;
            }
            else if(attempts == 2){
              Lockdown();
            }
          }
        }
      }
      //if one of the buttons gets pressed, go into lockdown
      else{
        Lockdown();
        delay(1000);
      }
    }
}

void system_Armed_outside(){
    delay(150);
    Serial.println("***Outside Alarm is active***");
    Serial.flush();
    retry_2:
    temp_Password = ""; //empty temp_password
    check_password = false;
 
    while(!check_password){
      if(!Outside_sensor_trigger()){ //if any of the triggers go off, go in lockdown mode
        //deactivate alarm by entering code
        key_pressed = keypad.getKey();
        //entering only numbers into temp_password
        if(key_pressed != NO_KEY){
          if( key_pressed == '0' || key_pressed == '1' || key_pressed == '2' || 
              key_pressed == '3' || key_pressed == '4' || key_pressed == '5' || 
              key_pressed == '6' || key_pressed == '7' || key_pressed == '8' || 
              key_pressed == '9'){
                temp_Password += key_pressed;
                Serial.print(key_pressed);
                Serial.flush();
                //tests
                /*Serial.println("____");
                Serial.println(temp_Password);
                Serial.println("____");*/
                //a small input delay
                delay(250);
          }
          //check the password when # is entered
          else if(key_pressed == '#'){
            //appearently having " " compared to "" enters an invisible white space making comparison impossible ... not shown on monitor
            if(password.equals(temp_Password)){
              Serial.println("Correct pincode");
              Serial.flush();
              alarm_system = 0; //turn off alarm
              buzz_correct();
              attempts = 0;
              check_password = true;
              Serial.println("***Alarm deactivated***");
              Serial.flush();
              break;
            }
            else if(password != temp_Password && attempts < 2){
              Serial.println("Incorrect pincode");
              Serial.flush();
              temp_Password = "";
              Led_Wrong();
              attempts += 1;
              goto retry_2;
            }
            else if(attempts == 2){
              Lockdown();
            }
          }
        }
      }
      //if the red button gets pressed, go into lockdown
      else{
        Lockdown();
        delay(1000);
      }
    }
}

void system_is_armed(){
    if(alarm_system == 1){
      //doesnt blink anymore
      LED_Alarm_Active();
      system_Armed_complete(); 
    }
    else if(alarm_system == 3){
      //doesnt blink anymore
      LED_Alarm_Active();
      system_Armed_outside();
    }
}

void system_Unarmed(){
    key_pressed = keypad.getKey();
    if(key_pressed == 'A'){
      alarm_system = 1;
      Serial.println("Option A selected");
      Serial.flush();
      buzz_led_option_A();
      system_Armed_complete();
    }
    else if(key_pressed == 'B'){
      alarm_system = 3;
      Serial.println("Option B selected");
      Serial.flush();
      buzz_led_option_B();
      system_Armed_outside();
    }
}
